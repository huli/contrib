val commonScalaVersion = "2.13.8"

lazy val prelude = RootProject(uri("https://gitlab.com/huli/prelude.git"))

lazy val contrib = (project in file("."))
  .dependsOn(prelude % "compile->compile;test->test")
  .settings(

  name         := "contrib",
  scalaVersion := commonScalaVersion,

  libraryDependencies ++= Seq(
    "org.scala-lang.modules" %% "scala-xml" % "2.0.1"
  ),

  Compile / scalaSource       := baseDirectory.value / "src",
  Test    / scalaSource       := baseDirectory.value / "test",
  Compile / resourceDirectory := baseDirectory.value / "resources",

  console / initialCommands   += "import contrib._",

  scalacOptions ++= Seq(
    "-encoding", "utf8",      //
    "-explaintypes",          //better type error messages
    "-language:_",            //enable all language features
    "-opt:_",                 //enable all optimizations
    "-Yno-imports",           //do not auto-import scala, java.lang, Predef
    "-deprecation",           //enable deprecation warnings
    "-feature",               //enable feature warnings
    "-unchecked",             //enable unchecked warnings
    "-opt-warnings:_",        //enable optimizer warnings
    "-Xfatal-warnings",       //warnings are errors
    "-Ywarn-dead-code",       //
    "-Ywarn-value-discard",   //
  ),

  Compile / console / scalacOptions --= Seq(
    "-opt:_",
    "-Yno-imports",
    "-Xfatal-warnings",
  ),
)

//======================================================================
scalapropsSettings
scalapropsVersion := "0.9.0"

//======================================================================
lazy val bench = (project in file ("bench"))
  .dependsOn(contrib)
  .enablePlugins(JmhPlugin)
  .settings(
    scalaVersion := commonScalaVersion,
    Compile / scalaSource := baseDirectory.value / "src"
)

// Build.sbt
