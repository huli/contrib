package contrib
package net
package wamp

import prelude._
import prelude.net.netty.{ Address, ws }

class Subscriber
  (a: Address, w: WAMP, s: prelude.net.Subscriber)
  extends ws.Subscriber(a, w.handler, s)

// Subscriber
