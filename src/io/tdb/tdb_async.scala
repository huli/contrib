package contrib
package io

import java.lang.{ Runnable, Thread, Throwable }
import java.util.concurrent.LinkedBlockingQueue
import prelude._, std._

object tdb_async {
  def open(root: String, fields: Array[String]): Task0[Long] =
    Task0 { tdb.open(root, fields) }

  def close(cons: Long): Task0[Unit] =
    Task0.async(WriterThread.close(cons), (_: Unit) => Task0.unit)

  def add
    (cons: Long, uuid: Bytes, timestamp: Long, values: Array[String])
    : Task0[Unit]
    = Task0.async(WriterThread.add(cons, uuid, timestamp, values),
                  (_: Unit) => Task0.unit)
}

//======================================================================
object WriterThread {
  val thread = {
    val t = new Thread(new Runnable { def run() = loop() })
    val h = new Thread.UncaughtExceptionHandler {
      def uncaughtException(t: Thread, e: Throwable) = unsafe.die(e) }
    t setDaemon true
    t setUncaughtExceptionHandler h
    t
  }

  def start() = thread.start()
  def stop()  = { thread.interrupt() ; thread.join() }

  final case class Op
    (label: String,
     thunk: ()   => Unit,
     succ : Unit => Unit,
     fail : EXN  => Unit)

  val queue          = new LinkedBlockingQueue[Op]
  def submit(op: Op) = { val true = queue offer op }

  @tailrec def loop(): Unit =
    if (Thread.interrupted) ()
    else queue.take match {
      case Op(label, thunk, succ, fail) =>
        EXN.Either(thunk()) match {
          case \/-(())  => succ(())
          case -\/(exn) => fail(exn) }
        loop() }

  def close(cons: Long)(succ: Unit => Unit, fail: EXN => Unit): Unit =
    submit(Op("close", () => tdb.close(cons), succ, fail))

  def add
    (cons: Long, uuid: Bytes, timestamp: Long, values: Array[String])
    (succ: Unit => Unit, fail: EXN => Unit)
    : Unit
    = submit(Op("add", () => tdb.add(cons, uuid, timestamp, values), succ, fail))
}

// tdb_async
